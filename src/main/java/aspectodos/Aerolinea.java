package aspectodos;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Aerolinea {

    static String categoria;

    public static void main(String[] args) {
        int[] vectorAsiento= new int[10];
        String clase = "";
        while (!(clase.equals("exit"))) {
            System.out.println("-- Categoria --");
            System.out.println("1. PRIMERA CLASE");
            System.out.println("2. CLASE ECONOMICA");
            BufferedReader in = getInputStream("Por favor, digite 1 para “Primera Clase“ o 2 para “Económica” o ingrese 'exit': ");
            try {
                clase = in.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (clase.equals("exit")) {
                break;
            }
            if(clase.equals("1")){
                boolean isFree = searchSeat(vectorAsiento, clase,0,5);
                if(!isFree){
                    in = getInputStream("No hay disponibilidad en “Primera Clase“ ingrese 2 para “Económica”");
                    try {
                        clase = in.readLine();
                        if (clase.equals("1")){
                            System.out.println("El siguiente vuelo parte en tres horas");
                            break;
                        }else if (clase.equals("exit")) {
                            break;
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if(!searchSeat(vectorAsiento, clase,5,10)){
                        System.out.println("El siguiente vuelo parte en tres horas");
                    }
                }
            }

            if(clase.equals("2")){
                boolean isFree = searchSeat(vectorAsiento, clase,5,10);
                if(!isFree){
                    in = getInputStream("No hay disponibilidad en “Económico” ingrese 1 para “Primera Clase“");
                    try {
                        clase = in.readLine();
                        if (clase.equals("2")){
                            System.out.println("El siguiente vuelo parte en tres horas");
                            break;
                        }
                        if (clase.equals("exit")) {
                            break;
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if(!searchSeat(vectorAsiento, clase,0,5)){
                        System.out.println("El siguiente vuelo parte en tres horas");
                    }
                }
            }
        }

        for(int i=0;i<vectorAsiento.length;i++){
            if(vectorAsiento[i]==1){
                System.out.println("Asiento vendido "+ (i+1) );
            }
        }
    }

    private static BufferedReader getInputStream(String mensaje) {
        System.out.println(mensaje);
        InputStreamReader convertir = new InputStreamReader(System.in);
        BufferedReader in = new BufferedReader(convertir);
        return in;
    }

    private static boolean searchSeat(int[] vectorAsiento, String clase,int fromIndex,int toIndex) {
        boolean libre=false;
        for(int asiento=fromIndex;asiento<toIndex;asiento++){
            if(vectorAsiento[asiento]==0){
                vectorAsiento[asiento]=1;
                libre=true;
                System.out.println("Asiento asignado " + (asiento+1) + ", en clase " + clase);
                break;
            }
        }
        return libre;
    }
}
