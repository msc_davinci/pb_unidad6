package aspectouno;

public class MainRectangulo {
    public static void main(String[] args) {
        /* Constructor sin Parametros , atributos igual a 1 */
        Rectangulo rect = new Rectangulo();
        rect.setAnchura(10.2);
        rect.setLongitud(12.2);/* Valor aceptado */
        System.out.printf("El área del rectangulo es %.2f y el perímetro es %.2f", rect.calcularArea(),rect.calculaPerimetro());
    }
}
